# TP SLAM5 - VueJS

  

## Introduction

Ce code a pour but principal l'apprentissage de VueJS ainsi que les API.   
La première étape a été de créer 4 API en s'appuyant sur des fonctionnalités déjà existantes telles que la liste des tâches, la création des tâches, la suppression des tâches, et mettre la tâche en tant que terminée.  
Dans un second temps, nous avons pu tester ces différentes API grâce à Postman, puis nous avons pu intégrer VueJS et utiliser sa librairie pour optimiser notre code (ainsi que l'utilisation de Fetch)

## Structure

- Dossier api : Regroupant les différentes api creation, liste, suppression et terminer

- Dossier libs : Regroupant les librairies pour le projet

- Dossier public : Regroupant les différents styles (CSS et autres) et fonctions (JS) du projet

- Fichier index.html : Index du projet contenant la structure de la page ainsi que les appels des différentes API.
