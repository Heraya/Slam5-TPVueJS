<?php

session_start();
header("content-type: application/json");

/*
 * Mark task as done
 */

$id = $_GET["id"];

// to-do in parameters ?
if($id != ""){

  // If to-do exist in session.
  if (array_key_exists($id, $_SESSION["todos"])){

    // Mark as done.
    $_SESSION["todos"][$id]["termine"] = true;
    echo json_encode(array("success" => true));
  }
  else {
    echo json_encode(array("success" => false));
  }
}
else {
    echo json_encode(array("success" => false));
}

?>
