<?php
session_start();

/*
 * Delete a task
 */
header("content-type: application/json");

$id = $_GET["id"];

// to-do in parameters ?
if($id != ""){

  // If to-do exist in session.
  if (array_key_exists($id, $_SESSION["todos"]) && $_SESSION["todos"][$id]["termine"]){
    unset($_SESSION["todos"][$id]);
    echo json_encode(array("success" => true));
  }
}
else {

echo json_encode(array("success" => false));

}

 ?>
